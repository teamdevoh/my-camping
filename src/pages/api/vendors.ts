// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import * as vendorService from '../../services/vendor.service'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<vendorService.IPaging>
) {
  
  const offset = Number(req.query.offset);
  const filterLength = Number(req.query.filterLength);
  const play = req.query.play.toString()
  const swimpool = req.query.swimpool.toString()
  const valley = req.query.valley.toString()
  const beach = req.query.beach.toString()
  const fishing = req.query.fishing.toString()
  const pet = req.query.pet.toString()

  
  const data = vendorService.getItems({ offset, filterLength, play, swimpool, valley, beach, fishing, pet });
  
  res.status(200).json(data)
}
