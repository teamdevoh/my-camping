import type { NextApiRequest, NextApiResponse } from 'next'
import * as vendorService from '../../services/vendor.service'

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<vendorService.ICategory[]>
) {
  const data = vendorService.getCategories();
  
  res.status(200).json(data)
}
