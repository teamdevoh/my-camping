import { NextApiRequest, NextApiResponse} from 'next'
import * as videoService from '../../services/video.service'


export default async function Handler (req: NextApiRequest, res: NextApiResponse) {
    const name = req.query.name.toString();
    const playlist = await videoService.getVideos({ query: name })
    
    res.status(200).json(playlist);
}