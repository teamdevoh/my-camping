import { GetStaticProps, NextPage } from 'next'
import { useCallback, useState } from 'react'
import { App, Shared, Vendor, Video } from '../components'
import { Container } from 'semantic-ui-react'

const Home: NextPage = () => {
  const [filters, setFilters] = useState<string[]>([])

  const [selectedItemTitle, setSelectedItemTitle] = useState('')
  const [showVideoLayer, setShowVideoLayer] = useState(false)

  const handleItemClick = useCallback((title) => {
    setSelectedItemTitle(title)
    setShowVideoLayer(true)
  }, [])

  const toggleVideos = useCallback(() => {
    setShowVideoLayer(!showVideoLayer)
  }, [showVideoLayer])

  const handleFilterClick = useCallback(
    (key: string) => {
      const newFilters = filters

      if (newFilters.includes(key)) {
        const index = newFilters.indexOf(key)
        newFilters.splice(index, 1)
      } else {
        newFilters.push(key)
      }

      setFilters([...newFilters])
    },
    [filters]
  )

  return (
    <>
      <App.DocHead />
      <App.Top>
        <Container textAlign="center">
          <Vendor.Filter onClick={handleFilterClick} />
        </Container>
      </App.Top>
      <App.Body>
        <Vendor.List filters={filters} onItemClick={handleItemClick} />
      </App.Body>
      <Shared.Modal
        open={showVideoLayer}
        onClose={toggleVideos}
        headerTitle="관련 영상"
        size="fullscreen"
      >
        <Video.Playlist query={selectedItemTitle} />
      </Shared.Modal>
    </>
  )
}

export default Home
