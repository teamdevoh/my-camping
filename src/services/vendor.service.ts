import data from '../data/camps.json'

export interface IVendor {
    title: string,
    img: string,
    address: string,
    play: string,
    swimpool: string,
    valley: string,
    beach: string,
    fishing: string,
    pet:string 
}

export interface ICategory {
    key: string,
    image: string,
    name: string
}

export interface IPaging {
    total : number,
    offset: number,
    limit: number,
    vendors: IVendor[]
}

type Props = {
    offset: number,
    filterLength: number, 
    play: string,
    swimpool: string,
    valley: string,
    beach: string,
    fishing: string,
    pet: string
}

export const getItems = ({ offset, filterLength, play, swimpool, valley, beach, fishing, pet }:Props) => {
    const limit = 50;
    
    const take = (offset + limit);
    
    const filteredData = data.filter(item => filterLength === 0 || ( (play === '0' || item.play === play) && 
    (swimpool === '0' || item.swimpool === swimpool) &&
    (valley === '0' || item.valley === valley) && 
    (beach === '0' || item.beach === beach) &&
    (fishing === '0' || item.fishing === fishing) && 
    (pet === '0' || item.pet === pet)) )
    
    const vendorResults: IPaging = {
        total: filteredData.length,
        limit: limit,
        offset: take,
        vendors: (filteredData as IVendor[]).slice(offset, take)
    }
    
    return vendorResults;
}

export const getCategories = () => {
    const data = [
        { "key": "play", "image": "playground.png", "name": "놀이시설" },
        { "key": "swimpool", "image": "swimmer.png", "name": "물놀이장" },
        { "key": "valley", "image": "road.png", "name": "계곡" },
        { "key": "beach", "image": "vacations.png", "name": "해수욕장" },
        { "key": "fishing", "image": "fishing.png", "name": "낚시" },
        { "key": "pet", "image": "pawprint.png", "name": "반려동물" }
      ];

    const categories = data as ICategory[];
    
    return categories;
}