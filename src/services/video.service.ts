import { searchVideo } from '../lib/search'

export interface Video {
    videoId: string,
    image: string,
    header: string,
    description: string,
    meta: string,
    extra: string
}

export const getVideos = async ({ query = '' }) => {
    const playlist = await searchVideo(query)
    return playlist; 
}