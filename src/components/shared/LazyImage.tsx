import useNativeLazyLoading from '@charlietango/use-native-lazy-loading'
import { useInView } from 'react-intersection-observer'

type Props = {
  alt: string
  src: string
}

const LazyImage = ({ alt, src }: Props) => {
  const supportsLazyLoading = useNativeLazyLoading()
  const { ref, inView } = useInView({
    triggerOnce: true,
    rootMargin: '200px 0px',
  })

  return (
    <div ref={ref} data-inview={inView} className="ui tiny image">
      {inView ? <img alt={alt} src={src} /> : null}
    </div>
  )
}

export default LazyImage
