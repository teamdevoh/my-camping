import { ReactNode } from 'react'
import { Button, Modal } from 'semantic-ui-react'

type Props = {
  children: ReactNode
  open?: boolean
  headerTitle: string
  size: 'mini' | 'tiny' | 'small' | 'large' | 'fullscreen'
  onOpen?: () => void
  onClose?: () => void
}

const CustomModal = ({
  children,
  headerTitle,
  open,
  size,
  onOpen,
  onClose,
}: Props) => {
  return (
    <Modal
      closeIcon
      size={size}
      open={open}
      onClose={onClose}
      onOpen={onOpen}
      style={{ height: '100%', right: '0%' }}
    >
      <Modal.Header>{headerTitle}</Modal.Header>
      <Modal.Content>{children}</Modal.Content>
      <Modal.Actions>
        <Button color="teal" onClick={onClose}>
          닫기
        </Button>
      </Modal.Actions>
    </Modal>

    // <Modal centered closeIcon open={open} onClose={onClose}>
    // <Modal.Content>
    //   <Embed
    //     id={videoId}
    //     placeholder={img}
    //     hd={true}
    //     source="youtube"
    //     autoplay={false}
    //   />
    // </Modal.Content>
    // </Modal>
  )
}

export default CustomModal
