import { ReactNode } from 'react'
import { Divider, Image } from 'semantic-ui-react'
// import { Head, Logo } from '.'

type Props = {
  children?: ReactNode
}

const Top = ({ children }: Props) => {
  return (
    <header>
      <Image alt="" src="/logo.png" size="small" />
      {children}
      <Divider />
    </header>
  )
}

export default Top
