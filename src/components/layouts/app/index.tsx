import DocHead from './DocHead'
import Top from './Top'
import Body from './Body'

export { DocHead, Top, Body }
