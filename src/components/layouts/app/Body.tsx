import { ReactNode } from 'react'
import { Container, Grid } from 'semantic-ui-react'

type Props = {
  children?: ReactNode
}

const Body = ({ children }: Props) => {
  return (
    <Container className="contents">
      <Grid stackable>
        <Grid.Column width={'sixteen'}>{children}</Grid.Column>
      </Grid>
    </Container>
  )
}

export default Body
