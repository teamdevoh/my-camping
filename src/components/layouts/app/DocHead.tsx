import NextHead from 'next/head'

const DocHead = () => {
  return (
    <NextHead>
      <title>캠핑</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </NextHead>
  )
}

export default DocHead
