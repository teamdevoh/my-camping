import axios from 'axios'
import { useEffect } from 'react'
import VendorList from './VendorList'
import { GetStaticProps } from 'next'

type Props = {
  data: any[]
}
const VendorListContainer = ({ data }: Props) => {
  const aa = data
  return (
    <div />
    // <VendorList />
  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  const res = await axios.get('/api/vendors')

  return {
    props: {
      data: res.data,
    },
  }
}

export default VendorListContainer
