import { useCallback } from 'react'
import { Icon, Item, Label } from 'semantic-ui-react'
import { isMobile } from 'react-device-detect'
import LazyImage from '../../shared/LazyImage'

type Props = {
  address: string
  title: string
  img: string
  play: string
  swimpool: string
  valley: string
  beach: string
  fishing: string
  pet: string
  onItemClick: (title: string) => void
}

const VendorListItem = ({
  address,
  title,
  img,
  play,
  swimpool,
  valley,
  beach,
  fishing,
  pet,
  onItemClick,
}: Props) => {
  const handleItemClick = useCallback(() => {
    onItemClick(address)
  }, [onItemClick])

  const labelSize = isMobile ? 'tiny' : 'small'

  return (
    <Item key={address}>
      <LazyImage
        alt=""
        src={`${
          img.length > 0
            ? img
            : 'https://react.semantic-ui.com/images/wireframe/image.png'
        }`}
      />
      <Item.Content>
        <Item.Header>{title}</Item.Header>
        <Item.Meta>{address}</Item.Meta>
        <Item.Description>
          <Icon
            name="youtube"
            color="red"
            size="large"
            style={{ cursor: 'pointer' }}
            onClick={handleItemClick}
          />
        </Item.Description>
        <Item.Extra>
          {play === '1' && (
            <Label image size={labelSize}>
              <img alt="" src="./playground.png" /> 놀이시설
            </Label>
          )}
          {swimpool === '1' && (
            <Label image size={labelSize}>
              <img src="./swimmer.png" /> 물놀이장
            </Label>
          )}
          {valley === '1' && (
            <Label image size={labelSize}>
              <img src="./road.png" /> 계곡
            </Label>
          )}
          {beach === '1' && (
            <Label image size={labelSize}>
              <img src="./vacations.png" /> 해수욕장
            </Label>
          )}
          {fishing === '1' && (
            <Label image size={labelSize}>
              <img src="./fishing.png" /> 낚시
            </Label>
          )}
          {pet === '1' && (
            <Label image size={labelSize}>
              <img src="./pawprint.png" /> 반려동물
            </Label>
          )}
        </Item.Extra>
      </Item.Content>
    </Item>
  )
}

export default VendorListItem
