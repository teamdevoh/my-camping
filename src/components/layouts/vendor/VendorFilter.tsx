import { useCallback, useEffect, useState } from 'react'
import { Label } from 'semantic-ui-react'
import svc from '../../../lib/service'
import { ICategory } from '../../../services/vendor.service'

const FilterLabel = ({
  value,
  name,
  onClick,
}: {
  value: string
  name: string
  onClick: (key: string) => void
}) => {
  const [toggle, setToggle] = useState(false)

  const handleClick = useCallback(() => {
    setToggle(!toggle)
    onClick(value)
  }, [setToggle, onClick, toggle, value])

  return (
    <Label
      key={name}
      active={toggle}
      color={toggle ? 'teal' : 'grey'}
      style={{
        width: '80px',
        marginBottom: '5px',
        textAlign: 'center',
        cursor: 'pointer',
      }}
      onClick={handleClick}
    >
      {name}
    </Label>
  )
}

const VendorFilter = ({ onClick }: { onClick: (name: string) => void }) => {
  const [categories, setCategories] = useState<ICategory[]>([])
  const getCategories = useCallback(async () => {
    const res = await svc.get('/api/categories')
    setCategories(res.data)
  }, [])

  useEffect(() => {
    getCategories()
  }, [getCategories])
  return (
    <>
      {categories.map((item) => (
        <FilterLabel
          key={item.name}
          value={item.key}
          name={item.name}
          onClick={onClick}
        />
      ))}
    </>
  )
}

export default VendorFilter
