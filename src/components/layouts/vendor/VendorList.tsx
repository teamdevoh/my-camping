import { useCallback, useEffect, useState } from 'react'
import { Item, Loader } from 'semantic-ui-react'
import { IPaging, IVendor } from '../../../services/vendor.service'
import ListItem from './VendorListItem'
import { useInView } from 'react-intersection-observer'
import svc from '../../../lib/service'

type Props = {
  filters: string[]
  onItemClick: (title: string) => void
}

let offset = 0
let total = 0
let initRender = true

const VendorList = ({ filters, onItemClick }: Props) => {
  const [vendors, setVendors] = useState<IVendor[]>([])
  const { ref, inView, entry } = useInView({
    threshold: 0,
    rootMargin: '200px 0px',
  })

  const getVendors = useCallback(async () => {
    const play = filters.includes('play') ? '1' : '0'
    const swimpool = filters.includes('swimpool') ? '1' : '0'
    const valley = filters.includes('valley') ? '1' : '0'
    const beach = filters.includes('beach') ? '1' : '0'
    const fishing = filters.includes('fishing') ? '1' : '0'
    const pet = filters.includes('pet') ? '1' : '0'

    const res = await svc.get(
      `/api/vendors?offset=${offset}&filterLength=${filters.length}&play=${play}&swimpool=${swimpool}&valley=${valley}&beach=${beach}&fishing=${fishing}&pet=${pet}`
    )
    const paging = res.data as IPaging
    if (paging.vendors.length > 0) {
      const paging = res.data as IPaging

      offset = paging.offset
      total = paging.total

      setVendors(vendors.concat(paging.vendors))
    }
  }, [vendors, filters])

  useEffect(() => {
    offset = 0
    setVendors([])
  }, [filters])

  useEffect(() => {
    if (
      initRender ||
      (inView && vendors.length < total && entry?.isIntersecting)
    ) {
      initRender = false
      getVendors()
    }
  }, [inView, vendors])

  return (
    <>
      <Item.Group divided unstackable>
        {vendors.map((camp: IVendor) => {
          return (
            <ListItem
              key={camp.title}
              address={camp.address}
              title={camp.title ? camp.title : ''}
              img={camp.img ? camp.img : ''}
              play={camp.play}
              swimpool={camp.swimpool}
              valley={camp.valley}
              beach={camp.beach}
              fishing={camp.fishing}
              pet={camp.pet}
              onItemClick={onItemClick}
            />
          )
        })}
        <div ref={ref} data-inview={inView}>
          <Loader active inline size="small" />
        </div>
      </Item.Group>
    </>
  )
}

export default VendorList
