import { Embed, Modal } from 'semantic-ui-react'

type Props = {
  videoId: string
  img: string
  onOpen: () => void
  onClose: () => void
  youtube: {
    playerVars: {
      autoplay: number
      controls: number
    }
  }
}

const Player = ({ videoId, img, onOpen, onClose }: Props) => {
  const open = videoId.length > 0 ? true : false
  return (
    <Modal centered closeIcon open={open} onClose={onClose}>
      <Modal.Content>
        <Embed
          id={videoId}
          placeholder={img}
          hd={true}
          source="youtube"
          autoplay={false}
        />
      </Modal.Content>
    </Modal>
  )
}

export default Player
