import { useCallback, useEffect, useState } from 'react'
import { Item } from 'semantic-ui-react'
import Player from './Player'
import service from '../../../lib/service'

const Playlist = ({ query }) => {
    const [playlist, setPlaylist] = useState([])
    const [selectedVideo, setSelectedVideo] = useState({ videoId: '', img: '' })
    
    const getItems = useCallback(async () => {
        const res = await service.get(`/api/videos?name=${query}`);
        setPlaylist(res.data ? res.data : []);
    }, [query]);

    useEffect(() => {
        getItems();
    }, [getItems, query])

    const handleItemClick = useCallback((videoId, img) => {
        setSelectedVideo({ videoId, img })
    }, [])

    const handleVideoClose = useCallback(() => {
        setSelectedVideo({ videoId: '', img: '' })
    }, [])

    return ( <>
    <Item.Group  divided unstackable>
        {playlist.map(item => {
            return (
            <Item key={item.id.videoId} onClick={() => {
                handleItemClick(item.id.videoId, item.snippet.thumbnails.high.url)
            }}>
                <Item.Image src={item.snippet.thumbnails.high.url} />
                <Item.Content>
                  <Item.Header as='a'>{item.title}</Item.Header>
                  <Item.Meta>{item.views}{item.snippet.publishedAt}</Item.Meta>
                  <Item.Description>
                    {item.description}
                  </Item.Description>
                  <Item.Extra>{item.duration_raw}</Item.Extra>
                </Item.Content>
              </Item>)
        })}
  </Item.Group>
  {selectedVideo && <Player videoId={selectedVideo.videoId} img={selectedVideo.img} onClose={handleVideoClose} />}</>)
}

export default Playlist;