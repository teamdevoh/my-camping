import { ParserVideoSource } from './parser';
import axios from 'axios';

export async function searchVideo(searchQuery: string) {
  const YOUTUBE_SEARCH = "https://www.youtube.com/results?app=desktop&search_query="
  const results = [];
  let details = [];
  let fetched = false;
  const options = { type: "video", limit: 0 };
  
  if(searchQuery.length === 0) {
    return;
  }
  
  const searchRes: any = await axios.get(YOUTUBE_SEARCH + encodeURIComponent(searchQuery))
  
  let html = searchRes.data;
  
  try {
    const data = html.split("ytInitialData = '")[0].split("';</script>")[0];
    
    html = data.replace(/\\x([0-9A-F]{2})/ig, (...items: string[]) => {
      return String.fromCharCode(parseInt(items[1], 16));
    });
    html = html.replaceAll("\\\\\"", "");
    html = JSON.parse(html)
  } catch(e) { /* nothing */}

  if(html && html.contents && html.contents.sectionListRenderer && html.contents.sectionListRenderer.contents
    && html.contents.sectionListRenderer.contents.length > 0 && html.contents.sectionListRenderer.contents[0].itemSectionRenderer &&
    html.contents.sectionListRenderer.contents[0].itemSectionRenderer.contents.length > 0){
    details = html.contents.sectionListRenderer.contents[0].itemSectionRenderer.contents;
    fetched = true;
  }
  // backup/ alternative parsing
  if (!fetched) {
    try {
      details = JSON.parse(html.split('{"itemSectionRenderer":{"contents":')[html.split('{"itemSectionRenderer":{"contents":').length - 1].split(',"continuations":[{')[0]);
      fetched = true;
    } catch (e) { /* nothing */
    }
  }
  if (!fetched) {
    try {
      details = JSON.parse(html.split('{"itemSectionRenderer":')[html.split('{"itemSectionRenderer":').length - 1].split('},{"continuationItemRenderer":{')[0]).contents;
      fetched = true;
    } catch(e) { /* nothing */ }
  }

  if (!fetched) return [];

  // tslint:disable-next-line:prefer-for-of
  for (let i = 0; i < details.length; i++) {
    if (typeof options.limit === "number" && options.limit > 0 && results.length >= options.limit) break;
    const data = details[i];

    const parserVideoSource = new ParserVideoSource();
    const parsed = parserVideoSource.parseVideo(data);
    if (!parsed) continue;
    const res = parsed;

    results.push(res);
  }

  return results;
}