
module.exports = {
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: '/videos/:search_query',
        destination: 'https://www.youtube.com/results?app=desktop&search_query=:search_query'
      }
    ]
  },
  webpack: (config, { isServer }) => {
    if (!isServer) {
         config.resolve.fallback = { fs: false, dns: false, net: false, tls: false };
    }

    return config;
  }
};